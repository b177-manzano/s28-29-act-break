const express = require("express");
const app = express();
const port = process.env.PORT || 4001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

let users = []
app.post("/register", (req, res) => {
	console.log(req.body);

	if(req.body.firstname !== '' 
		&& req.body.lastname !== ''
		 && req.body.username !== ''
		  && req.body.password !== ''){
		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered!`);
	}
	else{
		res.send("Please input all data.");
	}
});

app.post("/login",(req, res) => {

	let message;
	for(let i = 0; i < users.length;i++){
		if(req.body.username == users[i].username){
			users[i].password = req.body.password;
			message = `User ${req.body.username} is successfully login.`
			break;
		}
		else{
			message = "Invalid log in, check details";
		}
	}
	res.send(message);
});
app.listen(port, () => console.log(`Server running at port ${port}`));
